<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LAPAR BANGET</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <section id="header">
        <a href="#"><img src="img/logo-lrp-white4.png" class="logo" alt=""></a>

        <div>
            <ul id="navbar">
                <li><a href="index.php">Home</a></li>
                <li><a href="shop.php">Shop</a></li>
                <li><a href="about.php">About</a></li>
                <li><a class="active" href="contact.php">Contact</a></li>
                <li><a href="login.php"><i class="fa fa-door-open"></i></a></li>
                <a href="#" id="close"><i class="far fa-times"></i></a>
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>

    <section id="page-header2" class="about-header">
        <h2>Lets Talk</h2>
        <p>Leave a message, we love to hear from to you</p>
    </section>

    <section id="contact-details" class="section-p1">
        <div class="details">
            <span>GET IN TOUCH</span>
            <h2>Visit one of our store location or contact us today</h2>

            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.439830324497!2d106.80254331479087!3d-6.592127566274731!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c5d1420c8945%3A0x7a354e32b690f27d!2sJl.%20Pajajaran%2C%20RT.01%2FRW.04%2C%20Babakan%2C%20Kecamatan%20Bogor%20Tengah%2C%20Kota%20Bogor%2C%20Jawa%20Barat!5e0!3m2!1sen!2sid!4v1671068980402!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <br>
            <h3>Head Store</h3>
            <div>
                <li>
                    <i class="fal fa-map"></i>
                    <p>JL Pajajaran, Kota Bogor</p>
                </li>
                <li>
                    <i class="fal fa-envelope"></i>
                    <p>laparbanget@gmail.com</p>
                </li>
                <li>
                    <i class="fal fa-phone-alt"></i>
                    <p>0895365588314</p>
                </li>
                <li>
                    <i class="fal fa-clock"></i>
                    <p>Everyday!</p>
                </li>
            </div>
        </div>
    </section>

    <section id="form-details">
        <form action="">
            <span>LEAVE A MESSAGE</span>
            <h2>We love to hear from you</h2>
            <input type="text" placeholder="Your Name">
            <input type="text" placeholder="E-mail">
            <input type="text" placeholder="Subject">
            <textarea name="" id="" cols="30" rows="10" placeholder="Your Message"></textarea>
            <button class="normal">Submit</button>
        </form>

        <div class="people">
            <div>
                <img src="img/people/raditapict2.png" alt="">
                <p><span>Radita Febrianti</span> Senior Marketing Manager <br> Phone: 0895365588314 <br> E-mail: febriantiradita@gmail.com</p>
            </div>
            <div>
                <img src="img/people/aguspict2.jpg" alt="">
                <p><span>Agusrian Ilka</span> Senior Marketing Manager <br> Phone:  082383843255 <br> E-mail: agusrian@gmail.com</p>
            </div>
            <div>
                <img src="img/people/hafizpict2.jpg" alt="">
                <p><span>Hafiz Rizky</span> Senior Marketing Manager <br> Phone: 0889654861116 <br> E-mail: rizkyhafiz@gmail.com</p>
            </div>
            <div>
                <img src="img/people/ajengpict2.png" alt="">
                <p><span>Ajeng Puspita</span> Senior Marketing Manager <br> Phone: 081284271353 <br> E-mail: puspitaajeng@gmail.com</p>
            </div>
        </div>
    </section>


    <section id="newsletter" class="section-p1 section-m1">
        <div class="newstext">
            <h4>Sign Up For New Foods!</h4>
            <p>Get E-mail updates about our latest shop and<span> special food!</span>
            </p>
        </div>
        <div class="form">
            <input type="text" placeholder="Your E-mail address">
            <button class="normal">Sign Up</button>
        </div>
    </section>

    <footer class="section-p1">
        <div class="col">
            <img class="logo" src="img/logo-lrp-white4.png" alt="" style="padding-bottom: 20px">
            <h4>Contact</h4>
            <p><strong>Address: </strong> Jl. Pajajaran, Kabupaten Bogor</p>
            <p><strong>Phone: </strong> 0895365588314</p>
            <p><strong>Hours: </strong> 24/7</p>
            <div class="follow">
                <h4>Follow Us</h4>
                <div class="icon">
                    <i class="fab fa-facebook-f"></i>
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-instagram"></i>
                    <i class="fab fa-pinterest-p"></i>
                    <i class="fab fa-youtube"></i>
                </div>
            </div>
        </div>

        <div class="col">
            <h4>About</h4>
            <a href="#">About Us</a>
            <a href="#">Delivery Information</a>
            <a href="#">Privacy Policy</a>
            <a href="#">Terms & Condition</a>
            <a href="#">Contact Us</a>
            <a href="#">Help</a>
        </div>

        <div class="col install">
            <h4>Install App</h4>
            <p>From App Store or Google Play</p>
            <div class="row">
                <img src="img/pay/app.jpg" alt="">
                <img src="img/pay/play.jpg" alt="">
            </div>
            <p>Secured Payment Gateways</p>
            <img src="img/pay/pay.png" alt="">
        </div>

        <div class="copyright">
            <p>Copyright © 2022 Website Lapar Banget</p>
        </div>

    </footer>

    <script src="script.js"></script>
</body>
</html>