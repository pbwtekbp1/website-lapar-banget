<?php
    session_start();
    if($_SESSION['status_login'] != true){
        echo '<script>window.location="login.php"</script>';
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LAPAR BANGET</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <section id="header">
        <a href="#"><img src="img/logo-lrp-white4.png" class="logo" alt=""></a>

        <div>
            <ul id="navbar">
                <li><a class="active" href="dashboard.php">Home</a></li>
                <li><a href="profil.php">Profil</a></li>
                <li><a href="data-kategori.php">Data Category</a></li>
                <li><a href="data-produk.php">Data Product</a></li>
                <li><a href="logout.php"><i class="fa fa-door-closed"></i></a></li>
                <!-- <a href="#" id="close"><i class="far fa-times"></i></a> -->
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>

    <section id="hero">
        <h4>Welcome <?php echo $_SESSION['a_global']->admin_name?>!</h4>
        <br>
        <h1>ADMIN PAGE</h1>
        <br>
        <h2>Website</h2>
        <h2>Laman Antar</h2>
        <h2>Makanan Anget</h2>
        <p>Page admin website <strong>LAPAR BANGET</strong> only!</p>
    </section>

    <!-- <script src="script.js"></script> -->
</body>
</html>
