
<?php
    error_reporting(0);
    include 'db.php';
    $kontak = mysqli_query($conn, "SELECT admin_telp, admin_email, admin_address FROM tb_admin WHERE admin_id = 0");
    $a = mysqli_fetch_object($kontak);
    $produk = mysqli_query($conn, "SELECT * FROM tb_product WHERE product_id = '".$_GET['id']."' ");
    $p = mysqli_fetch_object($produk);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LAPAR BANGET</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <section id="header">
        <a href="#"><img src="img/logo-lrp-white4.png" class="logo" alt=""></a>

        <div>
            <ul id="navbar">
                <li><a href="index.php">Home</a></li>
                <li><a class="active" href="shop.php">Shop</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="login.php"><i class="fa fa-shopping-cart"></i></a></li>
                <a href="#" id="close"><i class="far fa-times"></i></a>
            </ul>
        </div>
        <div id="mobile">
            <!-- <a href="cart.html"><i class="fa fa-shopping-cart"></i></a> -->
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>

    <section id="prodetails" class="section-p1">
        <!-- <h2>Our Foods</h2>
        <p>Choose what you want!</p> -->
        <div class="single-pro-image">
            <img src="produk/<?php echo $p->product_image?>" id="MainImg" width="100%">
        </div>
        <div class="single-pro-details">
            <h4><?php echo $p->product_name ?></h4>
            <h2>Rp. <?php echo number_format($p->product_price) ?></h2>
            <a href="https://api.whatsapp.com/send?phone=<?php echo $a->admin_telp ?>&text=Hai, saya ingin membeli (tulis nama makanan dan jumlah porsi)." target="_blank"><button class="normal">Shop Now</button></a>
            <h4>Deskripsi</h4>
            <span><?php echo $p->product_description ?></span>
        </div>
    </section>

    <section id="product1" class="section-p1">
        <h2>New Foods</h2>
        <p>New foods for you</p>
        <div class="pro-container">
            <div class="box">

                <?php
                    $produk = mysqli_query($conn, "SELECT * FROM tb_product WHERE product_status = 1 ORDER BY product_id DESC");
                    if(mysqli_num_rows($produk) > 0){
                        while($p = mysqli_fetch_array($produk)){
                ?>
                    <a href="detail-product.php?id=<?php echo $p['product_id']?>">
                        <div class="col-5">
                            <img src="produk/<?php echo $p['product_image']?>">
                            <h5 class="nama"><?php echo substr($p['product_name'], 0, 30)?></h5>
                            <h4 class="harga">Rp. <?php echo number_format($p['product_price'])?></h4>
                            <div class="star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                    </a>
                <?php }} else{?>
                        <p>Produk Tidak Ada</p>
                <?php } ?>
            </div>
        </div>
    </section>

    <section id="newsletter" class="section-p1 section-m1">
        <div class="newstext">
            <h4>Sign Up For New Foods!</h4>
            <p>Get E-mail updates about our latest shop and<span> special food!</span>
            </p>
        </div>
        <div class="form">
            <input type="text" placeholder="Your E-mail address">
            <button class="normal">Sign Up</button>
        </div>
    </section>

    <footer class="section-p1">
        <div class="col">
            <img class="logo" src="img/logo-lrp-white4.png" alt="" style="padding-bottom: 20px">
            <h4>Contact</h4>
            <p><strong>Address: </strong> Jl. Kolonel Bustomi, Kabupaten Bogor</p>
            <p><strong>Phone: </strong> 0895365588314</p>
            <p><strong>Hours: </strong> 24/7</p>
            <div class="follow">
                <h4>Follow Us</h4>
                <div class="icon">
                    <i class="fab fa-facebook-f"></i>
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-instagram"></i>
                    <i class="fab fa-pinterest-p"></i>
                    <i class="fab fa-youtube"></i>
                </div>
            </div>
        </div>

        <div class="col">
            <h4>About</h4>
            <a href="#">About Us</a>
            <a href="#">Delivery Information</a>
            <a href="#">Privacy Policy</a>
            <a href="#">Terms & Condition</a>
            <a href="#">Contact Us</a>
            <a href="#">Help</a>
        </div>

        <div class="col install">
            <h4>Install App</h4>
            <p>From App Store or Google Play</p>
            <div class="row">
                <img src="img/pay/app.jpg" alt="">
                <img src="img/pay/play.jpg" alt="">
            </div>
            <p>Secured Payment Gateways</p>
            <img src="img/pay/pay.png" alt="">
        </div>

        <div class="copyright">
            <p>Copyright © 2022 Website Lapar Banget by Dita</p>
        </div>

    </footer>

    <script src="script.js"></script>
</body>
</html>