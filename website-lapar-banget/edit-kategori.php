<?php
    session_start();
    include 'db.php';
    if($_SESSION['status_login'] != true){
        echo '<script>window.location="login.php"</script>';
    }    

    $kategori = mysqli_query($conn, "SELECT * FROM tb_category WHERE category_id = '".$_GET['id']."' ");
    if(mysqli_num_rows($kategori) == 0){
        echo '<script>window.location="data-kategori.php"</script>';
    }
    $k = mysqli_fetch_object($kategori);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LAPAR BANGET</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <section id="header">
        <a href="#"><img src="img/logo-lrp-white4.png" class="logo" alt=""></a>

        <div>
            <ul id="navbar">
                <li><a href="index.php">Home</a></li>
                <li><a href="profil.php">Profil</a></li>
                <li><a class="active" href="data-kategori.php">Data Category</a></li>
                <li><a href="data-produk.php">Data Product</a></li>
                <li><a href="logout.php"><i class="fa fa-door-closed"></i></a></li>
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>

    <section id="heroprofil">
        <div class="container">
            <h2>Edit Data Category</h2>
            <br>
            <div class="box">
                <form action="" method="POST">
                    <input type="text" name="nama" placeholder="Nama Kategori" class="input-control" value="<?php echo $k->category_name ?>" required> 
                    <input type="submit" name="submit" value="Submit" class="btn" style="margin-bottom: 400px;"> 
                </form>
                <?php  
                    if(isset($_POST['submit'])){

                        $nama = ucwords($_POST['nama']);

                        $update = mysqli_query($conn, "UPDATE tb_category SET     
                                            category_name = '".$nama."'
                                            WHERE category_id = '".$k->category_id."' ");
                        if($update){
                            echo '<script>alert("Edit data succeed")</script>';
                            echo '<script>window.location="data-kategori.php"</script>';
                        }else{
                            echo 'data editing failed' .mysqli_error($conn);
                        }
                    }
                ?>
            </div>
        </div>
    </section>

    <!-- <script src="script.js"></script> -->
</body>
</html>
