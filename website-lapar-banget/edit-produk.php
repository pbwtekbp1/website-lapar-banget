<?php
    session_start();
    include 'db.php';
    if($_SESSION['status_login'] != true){
        echo '<script>window.location="login.php"</script>';
    }  
    
    $produk = mysqli_query($conn, "SELECT * FROM tb_product WHERE product_id = '".$_GET['id']."' ");
    $p = mysqli_fetch_object($produk);
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LAPAR BANGET</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>
<section id="header">
        <a href="#"><img src="img/logo-lrp-white4.png" class="logo" alt=""></a>

        <div>
            <ul id="navbar">
                <li><a href="index.php">Home</a></li>
                <li><a href="profil.php">Profil</a></li>
                <li><a href="data-kategori.php">Data Category</a></li>
                <li><a class="active" href="data-produk.php">Data Product</a></li>
                <li><a href="logout.php"><i class="fa fa-door-closed"></i></a></li>
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>

    <section id="heroprofil">
        <div class="container">
            <h2>Edit Data Product</h2>
            <div class="box">
                <form action="" method="POST" enctype="multipart/form-data">
                    <select class="input-control" name="kategori" required>
                        <option value="">--Choose--</option>
                        <?php
                            $kategori = mysqli_query($conn, "SELECT * FROM tb_category ORDER BY category_id DESC");
                            while($r = mysqli_fetch_array($kategori)){
                        ?>
                        <option value="<?php echo $r['category_id'] ?>" <?php echo ($r['category_id'] == $p->category_id)?'selected': ''; ?>><?php echo $r['category_name'] ?></option>
                        <?php } ?>
                    </select>

                    <input type="text" name="nama" class="input-control" placeholder="Product name" value="<?php echo $p->product_name ?>" required> 
                    <input type="text" name="harga" class="input-control" placeholder="Price" value="<?php echo $p->product_price?>" required> 
                    
                    <img src="produk/<?php echo $p->product_image ?>" width="100px">
                    <input type="hidden" name="foto" value="<?php echo $p->product_image ?>"> 
                    <input type="file" name="gambar" class="input-control"> 
                    <textarea class="input-control" name="deskripsi" placeholder="Description" value="<?php echo $p->product_description?>" required></textarea>
                    <br>
                    <select class="input-control" name="status">
                        <option value="">--Choose--</option>
                        <option value="1" <?php echo ($p->product_status == 1)? 'selected': ''; ?>>Stock still available</option>
                        <option value="0" <?php echo ($p->product_status == 0)? 'selected': ''; ?>>Out of stock</option>
                    </select>
                    <input type="submit" name="submit" value="Submit" class="btn"> 
                </form>
                <?php  
                    if(isset($_POST['submit'])){

                       //Data inputan dari form
                        $kategori  = $_POST['kategori'];
                        $nama      = $_POST['nama'];
                        $harga     = $_POST['harga'];
                        $deskripsi = $_POST['deskripsi'];
                        $foto      = $_POST['foto'];
                        $status    = $_POST['status'];
                    
                       // Data gambar yang baru
                        $filename = $_FILES['gambar']['name'];
                        $tmp_name = $_FILES['gambar']['tmp_name'];

                        
                       // Jika admin ganti gambar
                        if($filename != ''){
                            $type1 = explode('.', $filename);
                            $type2 = $type1[1];

                            $newname = 'produk'.time().'.'.$type2;

                            // Menampung data format file yang diizinkan
                            $tipe_diizinkan = array('jpg', 'jpeg', 'png', 'gif');
                            
                        //Validasi format file
                            if(!in_array($type2, $tipe_diizinkan)){
                                //Jika format file tidak ada di dalam tipe diizinkan
                                echo '<script>alert("File format not allowed")</script>';
                            }else{
                                unlink('./produk/'.$foto);
                                move_uploaded_file($tmp_name, './produk/'.$newname);
                                $namagambar = $newname;
                            }
                        }else{
                            //Jika admin tidak ganti gambar
                            $namagambar = $foto;
                        }
                        //Query update data produk
                        $update = mysqli_query($conn, "UPDATE tb_product SET
                                                category_id = '".$kategori."',
                                                product_name = '".$nama."',
                                                product_price = '".$harga."',
                                                product_description = '".$deskripsi."',
                                                product_image = '".$namagambar."',
                                                product_status =  '".$status."'
                                                WHERE product_id = '".$p->product_id."' ");
                        if($update){
                            echo '<script>alert("Change data successful!")</script>';
                            echo '<script>window.location="data-produk.php"</script>';
                        }else{
                            echo 'Change data failed'.mysqli_error($conn);
                        }
                       
                    }
                ?>
            </div>
        </div>
    </div>
    
    <!-- Footer -->
    <script>
        CKEDITOR.replace( 'deskripsi' );
    </script>
</body>
</html>
