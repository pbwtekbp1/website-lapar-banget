<?php
    session_start();
    include 'db.php';
    if($_SESSION['status_login'] != true){
        echo '<script>window.location="login.php"</script>';
    }    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LAPAR BANGET</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://cdn.ckeditor.com/4.20.0/standard/ckeditor.js"></script>
</head>

<body>

    <section id="header">
        <a href="#"><img src="img/logo-lrp-white4.png" class="logo" alt=""></a>

        <div>
            <ul id="navbar">
                <li><a href="index.php">Home</a></li>
                <li><a href="profil.php">Profil</a></li>
                <li><a class="active" href="data-kategori.php">Data Category</a></li>
                <li><a href="data-produk.php">Data Product</a></li>
                <li><a href="logout.php"><i class="fa fa-door-closed"></i></a></li>
            </ul>
        </div>
        <div id="mobile">
            <i id="bar" class="fas fa-outdent"></i>
        </div>
    </section>

    <section id="heroprofil">
        <div class="container">
            <h2>Add Product Data</h2>
            <div class="box">
                <form action="" method="POST" enctype="multipart/form-data">
                    <select class="input-control" name="kategori" required>
                        <option value="">--Choose--</option>
                        <?php
                            $kategori = mysqli_query($conn, "SELECT * FROM tb_category ORDER BY category_id DESC");
                            while($r = mysqli_fetch_array($kategori)){
                        ?>
                        <option value="<?php echo $r['category_id'] ?>"><?php echo $r['category_name'] ?></option>
                        <?php } ?>
                    </select>

                    <input type="text" name="nama" class="input-control" placeholder="Product name" required> 
                    <input type="text" name="harga" class="input-control" placeholder="Price" required> 
                    <input type="file" name="gambar" class="input-control" required> 
                    <textarea class="input-control" name="deskripsi" placeholder="Description"></textarea>
                    <br>
                    <select class="input-control" name="status">
                        <option value="">--Choose--</option>
                        <option value="1">Stock still available</option>
                        <option value="0">Out of stock</option>
                    </select>
                    <input type="submit" name="submit" value="Submit" class="btn"> 
                </form>
                <?php  
                    if(isset($_POST['submit'])){

                        // print_r($_FILES['gambar']);
                        // Menampung input dari form
                        $kategori  = $_POST['kategori'];
                        $nama      = $_POST['nama'];
                        $harga     = $_POST['harga'];
                        $deskripsi = $_POST['deskripsi'];
                        $status    = $_POST['status'];

                        // Menampung data file yg diupload
                        $filename = $_FILES['gambar']['name'];
                        $tmp_name = $_FILES['gambar']['tmp_name'];

                        $type1 = explode('.', $filename);
                        $type2 = $type1[1];

                        $newname = 'produk'.time().'.'.$type2;

                        // Menampung data format file yang diizinkan
                        $tipe_diizinkan = array('jpg', 'jpeg', 'png', 'gif');

                        // Validasi format file
                        if(!in_array($type2, $tipe_diizinkan)){
                            //Jika format file tidak ada di dalam tipe diizinkan
                            echo '<script>alert("File format not allowed")</script>';
                        }else{
                            //Jika format file sesuai dengan yang ada di dalam array tipe diizinkan
                            //Proses upload fle sekaligus insert ke database
                            move_uploaded_file($tmp_name, './produk/'.$newname);
                            $insert = mysqli_query($conn, "INSERT INTO tb_product VALUES (
                                        null, 
                                        '".$kategori."',
                                        '".$nama."',
                                        '".$harga."',
                                        '".$deskripsi."',
                                        '".$newname."',
                                        '".$status."',
                                        null
                                            ) ");
                            if($insert){
                                echo '<script>alert("Add data successful!")</script>';
                                echo '<script>window.location="data-produk.php"</script>';
                            }else{
                                echo 'Add data failed'.mysqli_error($conn);
                            }
                        }
                        
                    }
                ?>
            </div>
        </div>
    </div>
</body>
</html>
